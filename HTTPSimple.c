/*
AUTHOR: Abhijeet Rastogi (http://www.google.com/profiles/abhijeet.1989)

This is a very simple HTTP server. Default port is 10000 and ROOT for the server is your current working directory..

You can provide command line arguments like:- $./a.aout -p [port] -r [path]

for ex. 
$./a.out -p 50000 -r /home/
to start a server at port 50000 with root directory as "/home"

$./a.out -r /home/shadyabhi
starts the server at port 10000 with ROOT as /home/shadyabhi

*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include <stdbool.h>
#include<netdb.h>
#include<signal.h>
#include<fcntl.h>

#define CONNMAX 1000
#define BYTES 1024
#define SIZE 100
#define AUTH_DATA "picture.png"

char *ROOT;
int listenfd, clients[CONNMAX];
void error(char *);
void startServer(char *);
int respond(int);
char* base64Decoder(char*, int );


int main(int argc, char* argv[])
{
	struct sockaddr_in clientaddr;
	socklen_t addrlen;
	char c;    
	
	//Default Values PATH = ~/ and PORT=10000
	char PORT[6];
	ROOT = getenv("PWD");
	strcpy(PORT,"10000");

	int slot=0;

	//Parsing the command line arguments
	while ((c = getopt (argc, argv, "p:r:")) != -1)
		switch (c)
		{
			case 'r':
				ROOT = malloc(strlen(optarg));
				strcpy(ROOT,optarg);
				break;
			case 'p':
				strcpy(PORT,optarg);
				break;
			case '?':
				fprintf(stderr,"Wrong arguments given!!!\n");
				exit(1);
			default:
				exit(1);
		}
	
	printf("Server started at port no. %s%s%s with root directory as %s%s%s\n","\033[92m",PORT,"\033[0m","\033[92m",ROOT,"\033[0m");
	printf("============================================\n");
	printf("\n");
	// Setting all elements to -1: signifies there is no client connected
	int i;
	for (i=0; i<CONNMAX; i++)
		clients[i]=-1;
	startServer(PORT);
	// ACCEPT connections
	//while (1)
	for(int i =0;;i++)
	{
		addrlen = sizeof(clientaddr);
		clients[slot] = accept (listenfd, (struct sockaddr *) &clientaddr, &addrlen);
		if (clients[slot]<0)
			error ("accept() error");
		else
		{
			if ( fork()==0 )
			{
				int check = respond(slot);
				if(check>0)
				  return 1;
				exit(0);
			}
		}
		while (clients[slot]!=-1) slot = (slot+1)%CONNMAX;
	}

	return 0;
}

//start server
void startServer(char *port)
{
	struct addrinfo hints, *res, *p;

	// getaddrinfo for host
	memset (&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	if (getaddrinfo( NULL, port, &hints, &res) != 0)
	{
		perror ("getaddrinfo() error");
		exit(1);
	}
	// socket and bind
	for (p = res; p!=NULL; p=p->ai_next)
	{
		listenfd = socket (p->ai_family, p->ai_socktype, 0);
		if (listenfd == -1) continue;
		if (bind(listenfd, p->ai_addr, p->ai_addrlen) == 0) break;
	}
	if (p==NULL)
	{
		perror ("socket() or bind()");
		exit(1);
	}

	freeaddrinfo(res);

	// listen for incoming connections
	if ( listen (listenfd, 1000000) != 0 )
	{
		perror("listen() error");
		exit(1);
	}
}

// get file
void get_req_resource(const char* path, int client, char* auth_data) 
{
	char data_to_send[BYTES];
	int fd, bytes_read;
	
	//if (strncmp(&path[strlen(path)-strlen(AUTH_DATA)], AUTH_DATA, strlen(AUTH_DATA))==0)
	if (strstr(path,AUTH_DATA)!= NULL)
	{
		printf("Auth needed...\n");
		if (auth_data == NULL)
		{
			send(client, "HTTP/1.0 401 Unauthorized\n", 26, 0);
			send(client, "WWW-Authenticate: Basic realm=\"Realm\"\n",38,0);
			return;
		} else
			printf("Got Auth...%s\n", auth_data);
	}
	printf("file: %s\n", path);
	printf("============================================\n");
	printf("\n");
	if ( (fd=open(path, O_RDONLY))!=-1 )    //FILE FOUND
	{
		send(client, "HTTP/1.0 200 OK\n\n", 17, 0);
		while ( (bytes_read=read(fd, data_to_send, BYTES))>0 )
			write (client, data_to_send, bytes_read);
		close(fd);
	}
	else    write(client, "HTTP/1.0 404 Not Found\n", 23); //FILE NOT FOUND
}

char* base64Decoder(char encoded[], int len_str)
{
char* decoded_string;
decoded_string = (char*)malloc(sizeof(char) * SIZE);
int i, j, k = 0;
// stores the bitstream.
int num = 0;
// count_bits stores current
// number of bits in num.
int count_bits = 0;
// selects 4 characters from
// encoded string at a time.
// find the position of each encoded
// character in char_set and stores in num.
for (i = 0; i < len_str; i += 4)
{
num = 0, count_bits = 0;
for (j = 0; j < 4; j++)
{

// make space for 6 bits.
if (encoded[i + j] != '=')
{
num = num << 6;
count_bits += 6;
}

/* Finding the position of each encoded
character in char_set
and storing in "num", use OR
'|' operator to store bits.*/

// encoded[i + j] = 'E', 'E' - 'A' = 5
// 'E' has 5th position in char_set.
if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
num = num | (encoded[i + j] - 'A');

// encoded[i + j] = 'e', 'e' - 'a' = 5,
// 5 + 26 = 31, 'e' has 31st position in char_set.
else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
num = num | (encoded[i + j] - 'a' + 26);

// encoded[i + j] = '8', '8' - '0' = 8
// 8 + 52 = 60, '8' has 60th position in char_set.
else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
num = num | (encoded[i + j] - '0' + 52);

// '+' occurs in 62nd position in char_set.
else if (encoded[i + j] == '+')
num = num | 62;

// '/' occurs in 63rd position in char_set.
else if (encoded[i + j] == '/')
num = num | 63;

// ( str[i + j] == '=' ) remove 2 bits
// to delete appended bits during encoding.
else {
num = num >> 2;
count_bits -= 2;
}
}

while (count_bits != 0)
{
count_bits -= 8;

// 255 in binary is 11111111
decoded_string[k++] = (num >> count_bits) & 255;
}
}

// place NULL character to mark end of string.
decoded_string[k] = '\0';

return decoded_string;
}


struct pam_response *reply;

int function_conversation(int num_msg, const struct pam_message **msg, struct pam_response **resp, void *appdata_ptr)
{
  *resp = reply;
  return PAM_SUCCESS;
}
//client connection
int respond(int n)
{
	char mesg[99999], *reqline[3], path[99999];
	int rcvd;
	char *auth_data, *line, *save_ptr;
	memset( (void*)mesg, (int)'\0', 99999 );

	rcvd=recv(clients[n], mesg, 99999, 0);
	//printf("mssg: %s \n", mesg);

	if (rcvd<0)    // receive error
		fprintf(stderr,("recv() error\n"));
	else if (rcvd==0)    // receive socket closed
		fprintf(stderr,"Client disconnected unexpectedly.\n");
	else    // message received
	{
		printf("%s", mesg);
		reqline[0] = strtok_r (mesg, " \t\n", &save_ptr);
		if ( strncmp(reqline[0], "GET\0", 4)==0 )
		{
			reqline[1] = strtok_r (NULL, " \t", &save_ptr);
			reqline[2] = strtok_r (NULL, " \t\n", &save_ptr);
			if ( strncmp( reqline[2], "HTTP/1.0", 8)!=0 && strncmp( reqline[2], "HTTP/1.1", 8)!=0 )
			{
				write(clients[n], "HTTP/1.0 400 Bad Request\n", 25);
			}
			else
			{
				if ( strncmp(reqline[1], "/\0", 2)==0 )
					reqline[1] = "/index.html";        //Because if no file is specified, index.html will be opened by default (like it happens in APACHE...
				line = strtok_r (NULL, "\r\n", &save_ptr);
				while (line )
				{
					//printf("Next token: %s\n", line);
					auth_data = strstr(line, "Authorization");
					if (auth_data) break;
					line = strtok_r(NULL, "\r\n", &save_ptr);
				}
				printf ("AuthData: %s\n",auth_data);
				if (auth_data!=NULL)
				{
				
				char *encoded = strstr(line,"c");
				encoded+=2;
				printf ("Encoded Data: %s\n",encoded);
				printf ("Encoded Data Size: %d\n",strlen(encoded));
				char* decoded =  base64Decoder(encoded,strlen(encoded));
				char* decodedC = (char*)malloc(sizeof(decoded));
				strcpy(decodedC,decoded);
				printf ("Decoded Data: %s\n",decoded);
				char* password = strstr(decoded,":");
				char* login = strtok(decodedC, ":");
				printf ("Login: %s\n",login);
				password+=1;
				printf ("Password: %s\n",password);
			    
				const struct pam_conv local_conversation = { function_conversation, NULL };
				pam_handle_t *local_auth_handle = NULL; // this gets set by pam_start
				printf ("PAM handle created\n");
				int retval;
				// local_auth_handle gets set based on the service
				 retval = pam_start("common-auth", login, &local_conversation, &local_auth_handle);
				 reply = (struct pam_response *)malloc(sizeof(struct pam_response));
				 // *** Get the password by any method, or maybe it was passed into this function.
				 reply[0].resp = strdup(password);
				 reply[0].resp_retcode = 0;

				printf ("PAM started\n");
				if (pam_authenticate (local_auth_handle, 0) != PAM_SUCCESS){
				fprintf (stderr, "Ошибка аутентификации!\n");
				pam_end (local_auth_handle, 0);
				return 1;
				}
				else
				fprintf (stderr, "Аутентификация прошла успешно.\n");
				/* Закрываем контекст аутентификации */
				pam_end (local_auth_handle, 0);
				
				
				}
				strcpy(path, ROOT);
				strcpy(&path[strlen(ROOT)], reqline[1]);
	    
				get_req_resource(path,clients[n],auth_data);

			}
		}
	}

	//Closing SOCKET
	shutdown (clients[n], SHUT_RDWR);         //All further send and recieve operations are DISABLED...
	close(clients[n]);
	clients[n]=-1;
}

